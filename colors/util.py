__author__ = 'zalfonse'

from image.util import glob_spot, paint_box
from core import settings
import logging


def is_yellow(color):
    r, g, b = color

    r_to_g = r - g
    if r > 190 and g > 190:
        if r_to_g > 0:
            if r_to_g < 60 and b < 125:
                return True
        else:
            if abs(r_to_g) < 40 and b < 195:
                return True

    return False


def normalize_rgb(r_total, g_total, b_total):

    total = r_total + g_total + b_total
    if total != 0:
        r_norm = (float(r_total) / float(total)) * 255.0
        g_norm = (float(g_total) / float(total)) * 255.0
        b_norm = (float(b_total) / float(total)) * 255.0
    else:
        r_norm = 0
        g_norm = 0
        b_norm = 0

    return int(r_norm), int(g_norm), int(b_norm)


def get_color(pixel_list):
    r_total = 0
    g_total = 0
    b_total = 0
    for r, g, b in pixel_list:
        r_total += r
        g_total += g
        b_total += b
    return normalize_rgb(r_total, g_total, b_total)


def are_similar(color_1, color_2):
    r, g, b = color_1
    r2, g2, b2 = color_2

    return abs(r-r2) < 10 and abs(g-g2) < 10 and abs(b-b2) < 10


def get_colors_from_image(img):
    width, length = img.size

    sample_rate = int(.005 * length)
    if settings.debug:
        sample_rate = 1  # force high resolution sampling

    x_pixel = range(0, width, sample_rate)
    y_pixel = range(0, length, sample_rate)

    color_r = []
    color_g = []
    color_b = []
    color_y = []
    backup_color = []

    for x in x_pixel:
        for y in y_pixel:
            r, g, b = img.getpixel((x,y))
            brightness = (0.299*r + 0.587*g + 0.114*b)
            if brightness < settings.purity_threshold:
                if settings.debug:
                    glob_spot(img, x, y, (0, 0, 0))
                continue
            if is_yellow((r, g, b)):
                color_y.append((r, g, b))
                if settings.debug:
                    glob_spot(img, x, y, (255, 255, 0))
            elif r > (b + g) * settings.purity_threshold:
                color_r.append((r, g, b))
                if settings.debug:
                    glob_spot(img, x, y, (255, 0, 0))
            elif g > (r + b) * settings.purity_threshold:
                color_g.append((r, g, b))
                if settings.debug:
                    glob_spot(img, x, y, (0, 255, 0))
            elif b > (g + r) * settings.purity_threshold:
                color_b.append((r, g, b))
                if settings.debug:
                    glob_spot(img, x, y, (0, 0, 255))
            else:
                if settings.debug:
                    glob_spot(img, x, y)
                backup_color.append((r, g, b))

    color_arrays = [color_r, color_g, color_b, color_y]
    color_arrays.sort(key=len)
    color_arrays.reverse()

    if len(color_arrays[0]) != 0:
        r, g, b = get_color(color_arrays[0])
    else:
        r, g, b = get_color(backup_color)

    if len(color_arrays[1]) != 0:
        r_2, g_2, b_2 = get_color(color_arrays[1])
    else:
        r_2, g_2, b_2 = get_color(backup_color)

    if settings.debug:
        img = paint_box(img, r, g, b)
        img = paint_box(img, r_2, g_2, b_2, True)

    logging.info("Primary: {}, Secondary: {}".format((r, g, b), (r_2, g_2, b_2)))
    return (r, g, b), (r_2, g_2, b_2), img