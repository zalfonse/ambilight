__author__ = 'zalfonse'

from colors.util import get_colors_from_image, are_similar
from hue.api import change_lights
from core import settings

import logging


def make_changes(frame):
    color_p, color_s, img = get_colors_from_image(frame)
    img.show()
    change_lights(color_p)
    # if are_similar(color_p, history.pop()[0]):
    #     change_lights(color_p)
    #     color_combo = (color_p, color_s)
    # elif are_similar(color_s, history.pop()[0]):
    #     change_lights(color_s)
    #     color_combo = (color_s, color_p)
    # else:
    #     change_c += 1
    #     if change_c > 3:
    #         change_c = 0
    #         change_lights(color_p)
    #     color_combo = (color_p, color_s)

    logging.info("Changing color to {}".format(color_p))

    #return color_combo, change_c