__author__ = 'zalfonse'
import logging


from image import provider


#debug settings
debug = False
if debug:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

dry_run = True
if dry_run:
    logging.info("This is a dry run".format(dry_run))

#algorithm settings
darkness_threshold = 45
purity_threshold = .85
logging.info("Darkness threshold = {}, Purity Threshold = {}".format(darkness_threshold, purity_threshold))


#frame source
source = provider.IMAGE
samples_per_second = 2
logging.info("Samples per second = {}".format(samples_per_second))

#video or image to use if using video or file source
video_file = '../res/ponyo.avi'
image_file = '../res/2.jpg'

#hue settings
hue_ip = '192.168.3.33'
hue_user = 'newdeveloper'
light_ids = ['1']
logging.info("Hue bridge @ {}".format(hue_ip))