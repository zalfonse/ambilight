__author__ = 'zalfonse'

from beautifulhue.api import Bridge
from utils import rgb_to_xy

from core import settings

import logging


def change_lights(color):
    if not settings.dry_run:
        bridge = Bridge(device={'ip': settings.hue_ip}, user={'name': settings.hue_user})

    r, g, b = color
    logging.debug("Crafting command to Hue")

    if r + g + b > 10:
        x, y = rgb_to_xy(r, g, b)

        for i in settings.light_ids:
            resource = {
                'which': i,
                'data': {
                    'state': {
                        'on': True,
                        'xy': [x, y],
                        'bri': 180
                    }
                }
            }
            if not settings.dry_run:
                logging.debug("Command sent to Hue")
                bridge.light.update(resource)
    else:
        for i in settings.light_ids:
            resource = {
                'which': i,
                'data': {
                    'state': {
                        'on': True,
                        'bri': 10
                    }
                }
            }

            if not settings.dry_run:
                logging.debug("Command sent to Hue")
                bridge.light.update(resource)



