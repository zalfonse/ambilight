__author__ = 'zalfonse'

try:
    from PIL import ImageGrab
except ImportError:
    ImageGrab = None
    print "Unable to support Screenshot mode on OSX"

from PIL import Image
import cv2
import logging

SCREENSHOT = 0
WEBCAM = 1
IMAGE = 2


class Provider(object):

    def read(self):
        return None


class Screenshot(Provider):

    def read(self):
        img = ImageGrab.grab()
        return True, img

    def die(self):
        pass


class WebCam(Provider):

    def __init__(self):
        self.cap = cv2.VideoCapture(0)

    def read(self):
        ret, frame = self.cap.read()
        n_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        pil_im = Image.fromarray(n_frame)
        return ret, pil_im

    def die(self):
        self.cap.release()


class ImageFile(Provider):

    def __init__(self, file_loc):
        self.image = Image.open(file_loc)
        self.image = self.image.convert('RGB')

    def read(self):
        return True, self.image


providers = {
    SCREENSHOT: Screenshot,
    WEBCAM: WebCam,
    IMAGE: ImageFile
}