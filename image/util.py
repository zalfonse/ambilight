__author__ = 'zalfonse'


def glob_spot(img, x, y, color=(255, 255, 255), blur=False):
    img.putpixel((x, y), color)
    if blur:
        for i in range(1, 4, 2):
            for k in range(1, 4, 2):
                try:
                    img.putpixel((x - i, y - k), color)
                    img.putpixel((x + i, y - k), color)
                    img.putpixel((x - i, y + k), color)
                    img.putpixel((x + i, y + k), color)
                except IndexError:
                    pass

    return img


def paint_box(img, r, g, b, secondary=False):
    width, length = img.size

    box_x_edge = width-int(width * .1)
    box_y_edge = length-int(length * .1)

    if not secondary:
        for x in range(box_x_edge, width):
            for y in range(box_y_edge, length):
                if x - box_x_edge <= 2 or y - box_y_edge <= 2:
                    img.putpixel((x, y), (255, 255, 255))
                else:
                    img.putpixel((x, y), (r, g, b))
    else:
        box_x_edge = width-int(width * .05)
        box_y_edge = length-int(length * .05)

        for x in range(box_x_edge, width):
            for y in range(box_y_edge, length):
                if x - box_x_edge <= 2 or y - box_y_edge <= 2:
                    img.putpixel((x, y), (255, 255, 255))
                else:
                    img.putpixel((x, y), (r, g, b))

    return img
