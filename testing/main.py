__author__ = 'zalfonse'

import thread
import logging
import time

from image import provider
from core import settings, brain


logging.info("Begin")
if settings.source == provider.IMAGE:
    source = provider.providers[settings.source](settings.image_file)
else:
    source = provider.providers[settings.source]()

logging.debug("Provider {} instantiated".format(settings.source))

sleep_time = 1 / settings.samples_per_second

logging.info("Entering main loop")
while True:
    # Capture frame-by-frame
    ret, frame = source.read()
    logging.debug("Frame read")
    if settings.source != provider.IMAGE:
        logging.debug("Launching new thread to change colors")
        thread.start_new_thread(brain.make_changes, (frame,))
    else:
        logging.debug("Changing colors (blocking)")
        brain.make_changes(frame)
        break
    time.sleep(sleep_time)